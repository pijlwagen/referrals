package com.michel3951.scorchedmc.referral.Support;

import org.bukkit.Bukkit;

public class Logger {

    public static void info(String message) {
        System.out.println("[Referrals] " + message);
    }

    public static void error(String message) {
        Bukkit.getConsoleSender().sendMessage(Chat.colored("[Referrals] " + message));
    }
}
