package com.michel3951.scorchedmc.referral;

import com.michel3951.scorchedmc.referral.Commands.ReferralCommand;
import com.michel3951.scorchedmc.referral.Commands.UsesCommand;
import com.michel3951.scorchedmc.referral.Database.Sql;
import com.michel3951.scorchedmc.referral.Support.Chat;
import com.michel3951.scorchedmc.referral.Support.Logger;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import net.milkbowl.vault.economy.Economy;

import java.io.File;
import java.sql.Connection;

public final class Referral extends JavaPlugin {

    private static Economy econ;

    @Override
    public void onEnable() {
        Logger.info(Chat.colored("&aReferrals has been enabled"));

        File f = new File(this.getDataFolder() + "/");
        if (!f.exists()) f.mkdir();

        initializeEconomy();
        Sql.createDatabase();
        Connection conn = Sql.getConnection();

        getCommand("referral").setExecutor(new ReferralCommand(conn));
        getCommand("uses").setExecutor(new UsesCommand(conn));
    }

    @Override
    public void onDisable() {
        Logger.info(Chat.colored("&cReferrals has been disabled"));
    }

    private boolean initializeEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }

        econ = rsp.getProvider();
        return econ != null;
    }


    public static Economy getEconomy() {
        return econ;
    }
}
